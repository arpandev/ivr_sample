const express = require('express');
const debug = require('debug')('ivr-sample:api');
const xmlbuilder = require('xmlbuilder');

const router = express.Router();

/* Endpoint dealing with first ivr query: Male or Female */
router.get('/', (req, res) => {
  const { event, data } = req.query;
  res.type('text/xml');
  const xmlRoot = xmlbuilder.create('response',
    { version: '1.0', encoding: 'UTF-8' });
  debug(`Event: ${event}`);
  switch (event) {
    case 'NewCall':
      xmlRoot
        .ele('collectdtmf', { l: 1 })
        .ele('playtext', { quality: 'best' }, 'Please specify your Gender. Press 1 for male. Press 2 for female.');
      break;
    case 'GotDTMF':
      switch (data) {
        case '1':
          xmlRoot.ele('gotourl', {}, `http://${process.env.IP}/api/male`);
          break;
        case '2':
          xmlRoot.ele('gotourl', {}, `http://${process.env.IP}/api/female`);
          break;
        default:
          xmlRoot
            .ele('collectdtmf', { l: 1 })
            .ele('playtext', { quality: 'best' }, 'Invalid Option. Try Again. Press 1 for male. Press 2 for female.');
      }
      break;
    case 'Hangup':
      xmlRoot.ele('hangup');
      break;
    default:
  }
  const xmlString = xmlRoot.end();
  debug(xmlString);
  res.send(xmlString);
});

/* Endpoint dealing with second ivr query: Adult or Minor for males */
router.get('/male', (req, res) => {
  const { event, data } = req.query;
  res.type('text/xml');
  const xmlRoot = xmlbuilder.create('response',
    { version: '1.0', encoding: 'UTF-8' });
  debug(`Event: ${event}`);
  switch (event) {
    case undefined:
      xmlRoot
        .ele('collectdtmf', { l: 1 })
        .ele('playtext', { quality: 'best' }, 'Please specify your Age. Press 1 if you are above 21. Press 2 if you are below 21.');
      break;
    case 'GotDTMF':
      switch (data) {
        case '1':
          xmlRoot.ele('playtext', { quality: 'best' }, 'You are an adult.');
          xmlRoot.ele('hangup');
          break;
        case '2':
          xmlRoot.ele('playtext', { quality: 'best' }, 'Minors are not allowed.');
          xmlRoot.ele('hangup');
          break;
        default:
          xmlRoot
            .ele('collectdtmf', { l: 1 })
            .ele('playtext', { quality: 'best' }, 'invalid Option. Try Again. Press 1 if you are above 21. Press 2 if you are below 21.');
      }
      break;
    case 'Hangup':
      xmlRoot.ele('hangup');
      break;
    default:
  }
  const xmlString = xmlRoot.end();
  debug(xmlString);
  res.send(xmlString);
});

/* Endpoint dealing with second ivr query: Adult or Minor for females */
router.get('/female', (req, res) => {
  const { event, data } = req.query;
  res.type('text/xml');
  const xmlRoot = xmlbuilder.create('response',
    { version: '1.0', encoding: 'UTF-8' });
  debug(`Event: ${event}`);
  switch (event) {
    case undefined:
      xmlRoot
        .ele('collectdtmf', { l: 1 })
        .ele('playtext', { quality: 'best' }, 'Please specify your Age. Press 1 if you are above 18. Press 2 if you are below 18.');
      break;
    case 'GotDTMF':
      switch (data) {
        case '1':
          xmlRoot.ele('playtext', { quality: 'best' }, 'You are an adult.');
          xmlRoot.ele('hangup');
          break;
        case '2':
          xmlRoot.ele('playtext', { quality: 'best' }, 'Minors are not allowed.');
          xmlRoot.ele('hangup');
          break;
        default:
          xmlRoot
            .ele('collectdtmf', { l: 1 })
            .ele('playtext', { quality: 'best' }, 'invalid Option. Try Again. Press 1 if you are above 18. Press 2 if you are below 18.');
      }
      break;
    case 'Hangup':
      xmlRoot.ele('hangup');
      break;
    default:
  }
  const xmlString = xmlRoot.end();
  debug(xmlString);
  res.send(xmlString);
});

module.exports = router;
