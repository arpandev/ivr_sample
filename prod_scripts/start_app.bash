#!/bin/bash

# Establish path
source /home/ubuntu/.bashrc

# Change Working directory
cd /home/ubuntu/ivr

# start server
pm2 start ecosystem.config.js --env production

# save process list for unexpected shutdown
pm2 save
