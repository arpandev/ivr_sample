#!/bin/bash

# Establish path
source /home/ubuntu/.bashrc

# Change Working directory
cd /home/ubuntu/ivr

# Install node dependencies
npm install --production
npm audit fix

# Access permissions for node_modules folder
find /home/ubuntu/ivr/node_modules -type d -exec sudo chmod 777 {} \;
find /home/ubuntu/ivr/node_modules -type f -exec chmod 666 {} \;
