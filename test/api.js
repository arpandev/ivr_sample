// During the test the env variable is set to test
process.env.NODE_ENV = 'test';
process.env.IP = 'localhost';
process.env.PORT = 3000;

// Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const xmlParser = require('xml2json');
const server = require('../app');

const { expect } = chai;
chai.use(chaiHttp);
const ENDPOINT = '/api';
const requester = chai.request(server).keepOpen();

// Our parent block
describe('IVR API', () => {
  // Close Server after all tests are executed
  after(() => {
    requester.close();
  });

  // Index Endpoint
  describe('/GET index', () => {
    const url = ENDPOINT;
    it('New Call Event ', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'NewCall' });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('collectdtmf');
        const { collectdtmf } = response;
        expect(collectdtmf).to.be.an('object').that.has.all.keys('l', 'playtext');
        const { l } = collectdtmf;
        expect(l).to.be.equal('1');
      } catch (err) {
        throw err;
      }
    });

    it('User reply with 1', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'GotDTMF', data: 1 });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('gotourl');
      } catch (err) {
        throw err;
      }
    });

    it('User reply with 2', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'GotDTMF', data: 2 });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('gotourl');
      } catch (err) {
        throw err;
      }
    });

    it('User reply with invalid option', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'GotDTMF' });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('collectdtmf');
        const { collectdtmf } = response;
        expect(collectdtmf).to.be.an('object').that.has.all.keys('l', 'playtext');
        const { l } = collectdtmf;
        expect(l).to.be.equal('1');
      } catch (err) {
        throw err;
      }
    });

    it('User hung up the phone', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'Hangup' });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('hangup');
      } catch (err) {
        throw err;
      }
    });

    it('Call is disconnected', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'Disconnect' });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
      } catch (err) {
        throw err;
      }
    });
  });

  // Male Endpoint
  describe('/GET male', () => {
    const url = `${ENDPOINT}/male`;
    it('New Call Event ', async () => {
      try {
        const { status, text } = await requester.get(url);
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('collectdtmf');
        const { collectdtmf } = response;
        expect(collectdtmf).to.be.an('object').that.has.all.keys('l', 'playtext');
        const { l } = collectdtmf;
        expect(l).to.be.equal('1');
      } catch (err) {
        throw err;
      }
    });

    it('User reply with 1', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'GotDTMF', data: 1 });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('playtext', 'hangup');
      } catch (err) {
        throw err;
      }
    });

    it('User reply with 2', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'GotDTMF', data: 2 });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('playtext', 'hangup');
      } catch (err) {
        throw err;
      }
    });

    it('User reply with invalid option', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'GotDTMF' });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('collectdtmf');
        const { collectdtmf } = response;
        expect(collectdtmf).to.be.an('object').that.has.all.keys('l', 'playtext');
        const { l } = collectdtmf;
        expect(l).to.be.equal('1');
      } catch (err) {
        throw err;
      }
    });

    it('User hung up the phone', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'Hangup' });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('hangup');
      } catch (err) {
        throw err;
      }
    });

    it('Call is disconnected', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'Disconnect' });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
      } catch (err) {
        throw err;
      }
    });
  });

  // Female Endpoint
  describe('/GET index', () => {
    const url = `${ENDPOINT}/female`;
    it('New Call Event ', async () => {
      try {
        const { status, text } = await requester.get(url);
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('collectdtmf');
        const { collectdtmf } = response;
        expect(collectdtmf).to.be.an('object').that.has.all.keys('l', 'playtext');
        const { l } = collectdtmf;
        expect(l).to.be.equal('1');
      } catch (err) {
        throw err;
      }
    });

    it('User reply with 1', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'GotDTMF', data: 1 });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('playtext', 'hangup');
      } catch (err) {
        throw err;
      }
    });

    it('User reply with 2', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'GotDTMF', data: 2 });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('playtext', 'hangup');
      } catch (err) {
        throw err;
      }
    });

    it('User reply with invalid option', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'GotDTMF' });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('collectdtmf');
        const { collectdtmf } = response;
        expect(collectdtmf).to.be.an('object').that.has.all.keys('l', 'playtext');
        const { l } = collectdtmf;
        expect(l).to.be.equal('1');
      } catch (err) {
        throw err;
      }
    });

    it('User hung up the phone', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'Hangup' });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
        const { response } = parsedText;
        expect(response).to.be.an('object').that.has.all.keys('hangup');
      } catch (err) {
        throw err;
      }
    });

    it('Call is disconnected', async () => {
      try {
        const { status, text } = await requester.get(url).query({ event: 'Disconnect' });
        const parsedText = xmlParser.toJson(text, { object: true });
        expect(status).to.equal(200);
        expect(parsedText).to.have.all.keys('response');
      } catch (err) {
        throw err;
      }
    });
  });
});
