#!/bin/bash

# Establish path
source /home/ubuntu/.bashrc

# Change Working directory
cd /home/ubuntu/ivr

# Stop the service
pm2 stop ecosystem.config.js

# Clean previous deployment
cd ..
sudo rm -rf ivr
