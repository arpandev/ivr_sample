# IVR Sample

The aim of this project is to create an Interactive Voice Response(IVR) using KooKoo.in API.

## Application Flow Diagram

![Flowchart](https://bitbucket.org/arpandev/ivr_sample/raw/d0d8cce229fb64f8e7c41e4a37bf188a73b5de47/flowchart.png)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. Node.js & NPM

    Installing Node.js on Ubuntu 18.04 using NVM

    ```bash
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
    # If nvm command does not work after previous command then close and restart terminal
    nvm install --lts
    ```

2. Git [Optional]  
    Either you can download the code directly as zip/tarball or you can use Git to do the same.

    Installing Git on Ubuntu 18.04

    ```bash
    sudo apt update
    sudo apt install git
    ```

### Installing

A step by step series of examples that tell you how to get a development env running

1. Downloading Project files  
    Files can either be downloaded directly or using following command:

    ```bash
    git clone https://arpandev@bitbucket.org/arpandev/ivr_sample.git
    ```

2. Installing Dependencies

    ```bash
    npm install
    ```

3. Running Development Server

    ```bash
    npm start
    ```

In the browser use the following URL: http://localhost:3000/health  
This should return the following json

```json
{
    "online": "true"
}
```

## Running the tests

Explains how to run the automated tests for this system

### API tests

These check all the required test cases for the api using mocha and chai.js

```bash
npm test
```

### Coding style tests

These check for all coding guidelines using ESLint

```bash
npm run lint
```

## Deployment

The code already includes a ecosystem.config.js. This file lists all the necessary production env variables and their values. If using pm2, the file can be used directly as a configuration file for pm2 as follows:

```bash
pm2 start ecosystem.config.js --env production
```

For more details on pm2 refer http://pm2.keymetrics.io/ 

## Built With

* [Express.js](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js
* [NPM](https://www.npmjs.com/) - Dependency Management
* [ESLint](https://eslint.org) - Static analyzer and linter
* [Bitbucket](https://bitbucket.org) - Source Code/Version Control
* [AWS](https://aws.amazon.com) - Cloud Deployment
* [Kookoo.in](https://www.kookoo.in) - IVR handler/API

## Author

* **Arpan Jain**  
[GitHub](https://github.com/arpanjain97)  
[LinkedIn](https://linkedin.com/in/arpanjain97)

## Acknowledgments

* **[Billie Thompson](https://github.com/PurpleBooth)** - Wonderful README template
