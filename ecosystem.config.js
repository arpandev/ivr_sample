module.exports = {
  apps: [{
    name: 'ivr-sample',
    script: './bin/www',
    env_production: {
      NODE_ENV: 'production',
      PORT: '2000',
      IP: '52.66.117.153',
    },
  }],
};
