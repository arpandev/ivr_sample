// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

// Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');

const { assert } = chai;
chai.use(chaiHttp);

// Our parent block
describe('Health', () => {
  describe('/GET health', () => {
    it('it should GET health status', (done) => {
      chai.request(server)
        .get('/health')
        .end((err, res) => {
          assert.equal(res.status, 200);
          assert.isTrue(res.body.online);
          done();
        });
    });
  });
});
