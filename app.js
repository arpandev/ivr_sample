const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const logger = require('morgan');

// Setup defaults
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Get Required Routes
const apiRouter = require('./routes/api');

// Setup Express Application
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Setting Up NODE_ENV Specific Middleware Settings
function setupDevelopment() {
  app.use(logger('dev'));
}

function setupProduction() {
  app.use(logger('tiny'));
}

switch (process.env.NODE_ENV) {
  case 'test':
    break;
  case 'production':
    setupProduction();
    break;
  default:
    setupDevelopment();
}

// Health Endpoint
app.get('/health', (req, res) => {
  res.send({ online: true });
});

// Other Routes
app.use('/api', apiRouter);

module.exports = app;
